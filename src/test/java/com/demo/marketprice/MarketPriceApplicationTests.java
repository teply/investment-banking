package com.demo.marketprice;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.demo.marketprice.model.Price;
import com.demo.marketprice.service.PriceService;
import com.demo.marketprice.service.PriceServiceImpl;

import ch.qos.logback.core.status.Status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MarketPriceApplicationTests {

	@LocalServerPort
	private int port;
	
	private URL base;
	private PriceService priceService;
	
	@Autowired
	private TestRestTemplate template;
	
	
	@BeforeEach
	public void setUp() throws Exception{
		this.base = new URL("http://localhost:" + port + "/price/EUR/USD");
		
	}
	
	@Test
	public void getLatestPrice() throws Exception{
		priceService = new PriceServiceImpl();
		
		ResponseEntity<Price> response = template.getForEntity(base.toString(),
                Price.class);
		Price price = response.getBody();
		assertEquals("EUR/USD", price.getInstrumentName());
		
	}
	
	
	

}
