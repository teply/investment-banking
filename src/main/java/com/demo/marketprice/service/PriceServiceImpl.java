package com.demo.marketprice.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.demo.marketprice.model.Price;
import com.demo.marketprice.repository.MarketRepository;
import com.demo.marketprice.repository.MarketRepositoryImpl;
import com.demo.marketprice.utils.CsvReader;

/**
 * 
 * @author Mykhaylo.T
 *
 */
@RestController
public class PriceServiceImpl implements PriceService {

	private  MarketRepository marketRepository;
	private CsvReader csvReader;
	
	public PriceServiceImpl() {
		
		List<Price> marketPrices = CsvReader.getInstance().readCSV();
		marketRepository = new MarketRepositoryImpl(marketPrices);		
	}
	
	/**
	 * Return the latest price with the given instrument name specified after price;
	 * The endpoint must be like :
	 * http://localhost:8080/price/eur/usd
	 */
	@Override
	@GetMapping(path="/price/**", produces="application/json")
	public Price latestPrice(HttpServletRequest request) {
		String instrument = request.getRequestURI().split(request.getContextPath() + "/price/")[1]; 
	
		return marketRepository.findByInstrument(instrument.toUpperCase());
				
	}

}
