package com.demo.marketprice.service;

import javax.servlet.http.HttpServletRequest;

import com.demo.marketprice.model.Price;

public interface PriceService {

	Price latestPrice(HttpServletRequest request);
}
