package com.demo.marketprice.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.demo.marketprice.model.Price;
import com.demo.marketprice.notify.Subscriber;
import com.demo.marketprice.notify.Publisher;
import com.demo.marketprice.repository.MarketRepository;

/**
 * Read the csv file and for each new price apply margin and notify the subscribers.
 * @author Mykhaylo.T
 *
 */
public class CsvReader {

	private static final CsvReader INSTANCE = new CsvReader();
	
	MarketRepository marketRepository;
	List<Price> marketPrices;
	Publisher publisher;

	private CsvReader() {
		marketPrices = new ArrayList<>();
		publisher = new Publisher();
		publisher.registerListener(new Subscriber());
	}
	public static CsvReader getInstance() {
		return INSTANCE;
	}

	public List<Price> readCSV() {
		String line = "";
		String splitBy = ",";
		try {
			BufferedReader br = getFileFromResourceAsStream("marketPrices.csv");
			while ((line = br.readLine()) != null) {
				String[] splittedLine = line.split(splitBy);
				double bid = MarginPrice.applyMarginToPrice(Double.parseDouble(splittedLine[2]), true);
				double ask = MarginPrice.applyMarginToPrice(Double.parseDouble(splittedLine[3]), false);
				Date date = formatDate(splittedLine[4]);
				
				Price price = new Price(Integer.parseInt(splittedLine[0]), splittedLine[1], bid, ask, date);
				marketPrices.add(price);
				publisher.addPrice(price);
			}
			return getLatestPrices(marketPrices);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Given the all market prices, find the latest price.
	 * 
	 * @param marketPrices
	 * @return
	 */
	private List<Price> getLatestPrices(List<Price> marketPrices) {
		
		Map<String, Price> allMarketPrices = marketPrices.stream().collect(Collectors.toMap(Price::getInstrumentName,
				Function.identity(), BinaryOperator.maxBy(Comparator.comparing(Price::getTimestamp))));
		
		Comparator<Price> compareById = (p1, p2) -> Integer.compare(p1.getId(), p2.getId());

		return allMarketPrices.values().stream().sorted(compareById).collect(Collectors.toList());
	}
	
	/**
	 * Load csv file from resources
	 * @param filePath
	 * @return
	 */
	private BufferedReader getFileFromResourceAsStream(String filePath) {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(filePath);
		if (inputStream == null) {
			throw new IllegalArgumentException("file not found: " + filePath);
		} else {
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
			return br;
		}
	}
	
	/**
	 * Format date from price
	 * @param timestamp
	 * @return
	 */
	private Date formatDate(String timestamp) {
		try {
			Date date = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss:SSS").parse(timestamp);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
}
