package com.demo.marketprice.utils;
/**
 * Exception when the price will not be found.
 * @author Mykhaylo.T
 *
 */
public class PriceNotFoundExeption extends  RuntimeException {

	private static final long serialVersionUID = -7285981998050146687L;
	
	public PriceNotFoundExeption(String instrument) {
		super("Couldn't find market price: " + instrument);
	}

}
