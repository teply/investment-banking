package com.demo.marketprice.utils;
/**
 * Add margin to a single price(sell/buy)
 * @author Mykhaylo.T
 *
 */
public class MarginPrice {

	private static final double MARGIN = 0.1;
	
	private static double getMarginForPrice(double price) {
		return MARGIN * price;
	}
	
	public static double applyMarginToPrice(double price, boolean isBuy) {
		double margin = getMarginForPrice(price);
		double result = isBuy ? (price + margin) : (price - margin);
		return (double) Math.round(result * 10000d) / 10000d;
	}
}
