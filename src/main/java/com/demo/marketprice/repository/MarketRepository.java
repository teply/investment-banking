package com.demo.marketprice.repository;

import java.util.List;
import java.util.Optional;

import com.demo.marketprice.model.Price;

public interface MarketRepository {
	
	Price findByInstrument(String name);

}
