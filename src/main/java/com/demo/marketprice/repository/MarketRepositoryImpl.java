package com.demo.marketprice.repository;

import java.util.List;

import com.demo.marketprice.model.Price;
import com.demo.marketprice.utils.PriceNotFoundExeption;

public class MarketRepositoryImpl implements MarketRepository {
	
	private List<Price> marketPrices;
	
	public MarketRepositoryImpl(List<Price> marketPrices) {
		this.marketPrices = marketPrices;
	}
	
	@Override
	public Price findByInstrument(String name) {
	return marketPrices.stream()
				.filter(p -> p.getInstrumentName().equals(name))
				.findFirst()
				.orElseThrow(()->new PriceNotFoundExeption(name));
	}
	
}
