package com.demo.marketprice.notify;

import java.util.ArrayList;
import java.util.List;

import com.demo.marketprice.model.Price;
/**
 * For each new price market price notify all listeners
 * @author Mykhaylo.T
 *
 */
public class Publisher {
	
	List<Price> marketPrices = new ArrayList<>();
	List<Observer> listeners = new ArrayList<>();
	
	public void addPrice(Price price) {
		this.marketPrices.add(price);
		//notify the list of registered listeners(clients)
		this.notifyListeners(price);
	}
	
	/**
	 * add the listener to the list of the listeners
	 * @param listener
	 */
	public void registerListener(Observer listener) {
		this.listeners.add(listener);
	}
	public void unregisterListener(Observer listener) {
		this.listeners.remove(listener);
	}
	
	protected void notifyListeners(Price price) {
		this.listeners.stream().forEach(x -> x.onPriceAdded(price));
	}
}
