package com.demo.marketprice.notify;

import com.demo.marketprice.model.Price;

public interface Observer {

	public void onPriceAdded(Price price);
}
