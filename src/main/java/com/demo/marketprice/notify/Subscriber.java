package com.demo.marketprice.notify;

import com.demo.marketprice.model.Price;

public class Subscriber implements Observer{

	@Override
	public void onPriceAdded(Price price) {
		System.out.println("new price available : " + price.toString());
	}

}
