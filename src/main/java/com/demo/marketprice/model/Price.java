package com.demo.marketprice.model;

import java.io.Serializable;
import java.util.Date;

public class Price implements Serializable{

	private static final long serialVersionUID = -3518058418669283588L;
	
	private int id;
	private String instrumentName;
	private double sell;
	private double buy;
	private Date timestamp;
		
	public Price(int id, String instrumentName, double sell, double buy, Date timestamp) {
		super();
		this.id = id;
		this.instrumentName = instrumentName;
		this.sell = sell;
		this.buy = buy;
		this.timestamp = timestamp;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInstrumentName() {
		return instrumentName;
	}
	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}
	public double getSell() {
		return sell;
	}
	public void setSell(double sell) {
		this.sell = sell;
	}
	public double getBuy() {
		return buy;
	}
	public void setBuy(double buy) {
		this.buy = buy;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public String toString() {
		return "Price [id=" + id + ", instrumentName=" + instrumentName + ", sell=" + sell + ", buy=" + buy
				+ ", timestamp=" + timestamp + "]";
	}
}
